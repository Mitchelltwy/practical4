﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practical4
{
    class BankAccount
    {
        /*
        BankAccount
        -accNo: string
        -accName: string
        -balance: double
        +BankAccount()
        +BankAccount(string, string, double)
        +Deposit(double)
        +Withdraw(double) :bool
        +ToString() :string
        */

        public string AccNo { get; set; }
        public string AccName { get; set; }
        public double Balance { get; set; }
        public BankAccount() { }
        public BankAccount(string no, string na, double b)
        {
            AccNo = no;
            AccName = na;
            Balance = b;
        }

        public void Deposit(double amount)
        {
            Balance = Balance + amount;
        }

        public bool Withdraw(double amount)
        {
            if (amount <= Balance)
            {
                Balance -= amount;
                return true;
            }
            else
            {
                return false;
            }
        }

        //(1) ToString() superclass
        public override string ToString()
        {
            return "AccNo:" + AccNo + "\tAccName:" + AccName + "\tBalance:" + Balance;
        }



    }
}

