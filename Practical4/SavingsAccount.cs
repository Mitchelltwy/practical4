﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practical4
{
/*
-rate: double
+SavingsAccount()
+SavingsAccount(string, string, double, double)
+CalculateInterest(): double
+ToString():string
*/
    class SavingsAccount : BankAccount
    {
        public double Rate { get; set; }
        public SavingsAccount() : base() { }


        //+SavingsAccount(string, string, double, double)
        public SavingsAccount(string no, string na, double b, double r) : base(no, na, b) //getting the superclass to do number base and balance
        {
            // only 1 line here
            Rate = r; //declare cause subclass doing
        }

        /*CalculateInterest() calculates and returns the interest based on the account  balance and interest rate using the formula 
          Balance * Interest rate / 100.*/
        public double CalculateInterest()
        {
            double interest = Balance * Rate / 100;
            return interest;
        }

        //(2) ToString() subclass
        public override string ToString()
        {
            //"AccNo ... AccName ... Balance"
            return base.ToString() + "\tRate:" + Rate;
        }



    }
}
