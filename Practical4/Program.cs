﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Practical4
{
    class Program
    {
        static void Main(string[] args)
        {
            //create a list to store savings accounts
            List<SavingsAccount> savingsAccList = new List<SavingsAccount>();
            //call method to fill the list with data from a file
            ReadFromFile(savingsAccList);
            string option = "";

            while (option != "0")
            {
                DisplayMenu();
                Console.Write("Enter option: ");
                option = Console.ReadLine();
                if (option == "1")
                {
                    DisplayAll(savingsAccList);
                }
                else if (option == "2")
                {
                    Deposit(savingsAccList);
                }
                else if (option == "3")
                {
                    Withdraw(savingsAccList);
                }
                else if (option == "4")
                {
                    DisplayDetails(savingsAccList);
                }
                else if (option == "0")
                {
                    Console.WriteLine("---------");
                    Console.WriteLine("Goodbye");
                    Console.WriteLine("---------");
                    break;
                }

            }

            Console.ReadKey();
        }


        static void ReadFromFile(List<SavingsAccount> accountList)
        {
            string[] lines = File.ReadAllLines("savings_account(3).csv");
            for (int i = 1; i < lines.Length; i++)
            {
                string[] data = lines[i].Split(',');
                string no = data[0];
                string name = data[1];
                double balance = Convert.ToDouble(data[2]);
                double rate = Convert.ToDouble(data[3]);
                SavingsAccount acc = new SavingsAccount(no, name, balance, rate);
                accountList.Add(acc);
            }
        }



        static void DisplayMenu()
        {
            Console.WriteLine();
            Console.WriteLine("Menu");
            Console.WriteLine("[1] Display all accounts");
            Console.WriteLine("[2] Deposit");
            Console.WriteLine("[3] Withdraw");
            Console.WriteLine("[0] Exit");
        }



        static void DisplayAll(List<SavingsAccount> sList)
        {
            foreach (SavingsAccount s in sList)
            {
                Console.WriteLine(s);
            }
        }


        static SavingsAccount SearchAcc(List<SavingsAccount> sList, string accNo)
        {
            foreach (SavingsAccount s in sList)
            {
                if (accNo == s.AccNo)
                {
                    return s;
                }
            }
            return null;
        }



        static void Deposit(List<SavingsAccount> sList)
        {
            Console.Write("Enter the Account Number: ");
            string accNo = Console.ReadLine();
            Console.Write("Amount to deposit: ");
            double amount = Convert.ToDouble(Console.ReadLine());
            SavingsAccount acc = SearchAcc(sList, accNo);
            if (acc == null)
            {
                Console.WriteLine("Account not found");
            }
            else
            {
                acc.Deposit(amount);
                Console.WriteLine(amount + " " + "deposited successfully");
                Console.WriteLine("Acc No: {0} Acc Name: {1} Balance: {2} Rate: {3}",
                        acc.AccNo, acc.AccName, acc.Balance, acc.Rate);
            }
        }



        static void Withdraw(List<SavingsAccount> sList)
        {
            Console.Write("Enter the Account Number: ");
            string accNo = Console.ReadLine();

            SavingsAccount acc = SearchAcc(sList, accNo);
            if (acc != null)
            {
                Console.Write("Amount to withdraw: ");
                double amount = Convert.ToDouble(Console.ReadLine());

                if (acc.Withdraw(amount))
                {
                    Console.WriteLine(amount + " " + "withdraw successfully");
                    Console.WriteLine("Acc No: {0} Acc Name: {1} Balance: {2} Rate: {3}",
                        acc.AccNo, acc.AccName, acc.Balance, acc.Rate);
                }
                else
                {
                    Console.WriteLine("Insufficient funds");
                }

            }
            else
            {
                Console.WriteLine("Unable to find account number. Please try again.");
            }
        }


        static void DisplayDetails(List<SavingsAccount> sList)
        {
            Console.WriteLine("{0, -10} {1, -15} {2, -15} {3, -15} {4, -15}", "Acc No", "Acc Name", "Balance", "Rate", "Interest amt");
            foreach (SavingsAccount s in sList)
            {
                Console.WriteLine("{0, -10} {1, -15} {2, -15} {3, -15} {4, -15}", s.AccNo, s.AccName, s.Balance, s.Rate, s.CalculateInterest());
            }
        }

    }
}